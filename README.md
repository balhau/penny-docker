# Penny-Docker

## Overview
  Here you find all the Docker files regarding the Penny Project.

## Containers

### Utilities

Then you should run *docker-compose build* followed by *docker-compose up* to build and start a docker environment

Run *docker ps -a* to see the containers on and also to check for *port mapping*.

To scale up or down a specific set of machines you'll need to do something like

    docker-compose scale elasticsearch=3

to scale up and

    docker-compose scale elasticsearch=1

to scale down. Notice that you can do the same for the other images you've got declared on the *docker-compose.yml* file

### Penny-Web

To start the penny web application the *docker-compose.yml* should include the following lines

    penny-web:
      build: ./penny-web
      ports:
        - "5000"

### penny-backend

  To start the backend component of penny you need to create the following, or comment, in the *docker-compose.yml* file.

    penny-backend:
      links:
        - elasticsearch
      build: ./penny-backend
      ports:
        - "3000"

Notice the *links* entry with a reference for *elasticsearch*. This has, as a main purpose, give automatic networking resolution for the elastic search container.

### Elastic Search

To start the elastic search container you just need to add (if not already done) the following block of code to your *docker-compose.yml* file

    elasticsearch:
      build: ./elasticsearch
      ports:
        - "9200"
        - "9300"


### Notes

All the ports here are mapped for random host ports. If, for example, you want to force the penny-backend mapping port you could replace *"3000"* for *"4000:3000"* which will map the container *3000* port into the host port *4000*, notice that the port numbers could be the same.
